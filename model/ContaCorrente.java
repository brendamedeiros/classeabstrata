package model;

public class ContaCorrente extends Conta {
	private double saldo;

	public ContaCorrente(double saldo) {
		super(saldo);
	}

	public void atualiza(double taxa) {
		this.saldo += this.saldo * taxa * 2;
	}
	
}
