package model;

public class ContaPoupanca extends Conta{
	
	private double saldo;
	
	public ContaPoupanca(double saldo) {
		super(saldo);
	}

	//Metodo foi reescrito apos o erro
	public void atualiza(double taxa) {
		this.saldo += this.saldo * taxa * 3;		
	}

	/*
	 * public void atualiza(double taxa) {
		this.saldo += this.saldo * taxa * 3;
	}
	*/
	

}
