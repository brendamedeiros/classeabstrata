package model;

public abstract class Conta {
	private double saldo;
	private String numero;
	
	//Metodo construtor
	
	public Conta(double saldo) {
		this.saldo = saldo;
	}
	
	//Metodos acessadores
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getSaldo() {
		return this.saldo;
	}
	
	//Outros metodos

	public void deposita(double valor) {
		this.saldo += valor;
	}
	
	public void saca(double valor) {
		this.saldo -= valor;
	}

	public abstract void atualiza(double taxa);
	
}
